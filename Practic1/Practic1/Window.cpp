#include <Windows.h>

LRESULT CALLBACK SoftwareMainProcedure(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp);
WNDCLASS NewWindowClass(HBRUSH BGColor, HCURSOR Cursor, HINSTANCE hInst, HICON Icon, LPCWSTR Name, WNDPROC Procedure);


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR args, int ncmdshow)
{
	WNDCLASS SoftwareMainClass = NewWindowClass((HBRUSH)COLOR_WINDOW, LoadCursor(NULL, IDC_ARROW), hInst, LoadIcon(NULL, IDI_QUESTION), L"MainWndClass", SoftwareMainProcedure);
	
	if (!RegisterClassW(&SoftwareMainClass)) { return -1; }
	MSG SoftwareMainMessage = { 0 };

	CreateWindow(L"MainWndClass", L"First c++ Window", WS_OVERLAPPEDWINDOW | WS_VISIBLE, 100, 100, 500, 500, NULL, NULL, NULL, NULL);
	while (GetMessage(&SoftwareMainMessage, NULL, NULL, NULL)) 
	{ 
		TranslateMessage(&SoftwareMainMessage);
		DispatchMessage(&SoftwareMainMessage);
	}
}

WNDCLASS NewWindowClass(HBRUSH BGColor, HCURSOR Cursor, HINSTANCE hInst, HICON Icon, LPCWSTR Name, WNDPROC Procedure)
{
	WNDCLASS NWC = { 0 };

	NWC.hCursor = Cursor;
	NWC.hIcon = Icon;
	NWC.hInstance = hInst;
	NWC.lpszClassName = Name;
	NWC.lpfnWndProc = Procedure;
	NWC.hbrBackground = (HBRUSH)CreateSolidBrush(RGB(255, 255, 255));

	return NWC;
}

LRESULT CALLBACK SoftwareMainProcedure(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp)
{

	switch (msg) {
	case WM_PAINT: {
		
		
		/* ������� "*.BMP" ����������� �� �������� ���������: */
		//HBITMAP hBitmap = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_BITMAP1));
		/* ������� "*.BMP" ����������� c ������� �����: */
		HBITMAP hBitmap = (HBITMAP)LoadImage(NULL, TEXT("E:\\lp.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		InvalidateRect(hwnd, NULL, TRUE);
		PAINTSTRUCT ps;
		HDC hdc, hdcMem;
		hdc = BeginPaint(hwnd, &ps);
		RECT rc;
		GetClientRect(hwnd, &rc);
		hdcMem = CreateCompatibleDC(hdc);
		SelectObject(hdcMem, hBitmap);
		BITMAP bmp;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);
		/* ------------------------------------ /
		/ ������� ��-������� ����:
		/ ------------------------------------ */
		//StretchBlt(hdc,0,0,rc.right-rc.left,rc.bottom-rc.top,hdcMem,0,0,bmp.bmWidth,bmp.bmHeight,SRCCOPY);
		//StretchBlt(hdc, 0, 0, rc.right, rc.bottom, hdcMem, 0, 0, bmp.bmWidth, bmp.bmHeight, SRCCOPY);
		BOOL m_bTile = TRUE;
		if (m_bTile)
		{ /* ������� � ������ ����: */
			BitBlt(hdc, ((rc.right - rc.left) / 2) - (bmp.bmWidth / 2),
				((rc.bottom - rc.top) / 2) - (bmp.bmHeight / 2),
				bmp.bmWidth, bmp.bmHeight, hdcMem, 0, 0, SRCCOPY);
		}

		DeleteDC(hdcMem);
		EndPaint(hwnd, &ps);
	}
				 break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
		
	default: return DefWindowProc(hwnd, msg, wp, lp);
	}
}